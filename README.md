As Chief Financial Officer, Tony is responsible for the overall finance function of Rogers. He also oversees corporate strategy and financial services. Since joining the company in April 2012, Tony has focused on driving share value growth and, in particular, the operational execution of the company from a financial perspective.


Website: http://www.rogers.com
